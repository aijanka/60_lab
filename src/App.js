import React, {Component} from 'react';
import Wrapper from "./hoc/Wrapper";
import SendMessageForm from "./components/SendMessageForm/SendMessageForm";
import MessagePanel from "./components/MessagePanel/MessagePanel";

class App extends Component {
    state = {
        currentMessage: '',
        currentAuthor: '',
        posts: []
    };

    interval = '';

    getPosts(url) {
        return fetch(url)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Something went wrong with network request');
            })
            .then(posts => {
                this.setState({posts});
            })
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    };

    componentWillMount() {

        this.getPosts('http://146.185.154.90:8000/messages');

    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    componentDidMount() {
        this.interval = setInterval(() => this.getPosts('http://146.185.154.90:8000/messages'), 5000);
        this.scrollToBottom();
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    shouldComponentUpdate(nextState, nextProps){
        return nextState !== this.state;
    }

    addMessageToServer() {
        const data = new URLSearchParams();
        data.append('message', this.state.currentMessage);
        data.append('author', this.state.currentAuthor);

        const config = {
            method: 'POST',
            body: data
        };

        fetch('http://146.185.154.90:8000/messages', config)
            .then(response => this.interval = setInterval(() => this.getPosts('http://146.185.154.90:8000/messages'), 5000));

    }

    saveCurrentMessage(event) {
        console.log(event.target.value);
        this.setState({currentMessage: event.target.value});
    }

    saveCurrentAuthor(event) {
        console.log(event.target.value);
        this.setState({currentAuthor: event.target.value});
    }

    render() {
        return (
            <Wrapper>

                <SendMessageForm
                    currentMessage={(event) => this.saveCurrentMessage(event)}
                    currentAuthor={(event) => this.saveCurrentAuthor(event)}
                    addPost={() => this.addMessageToServer()}
                />


                {this.state.posts.map((post, index) => (
                        <MessagePanel
                            key={(Date.now() + index).toString()}
                            datetime={post.datetime}
                            message={post.message}
                            author={post.author}
                        />
                    )
                )}
                <div style={{ float:"left", clear: "both" }} ref={(el) => { this.messagesEnd = el; }}/>

            </Wrapper>
        );
    }
}

export default App;
