import React from 'react';
import Panel from "react-bootstrap/es/Panel";
import Moment from 'react-moment';
import 'moment-timezone';

const MessagePanel = props => {
    return (
        <Panel  bsStyle="success">
            <Panel.Heading>
                <Panel.Title componentClass="h3">
                    <Moment tz='Asia/Almaty' type="datetime-local">{props.datetime}</Moment> : {props.author}
                </Panel.Title>
            </Panel.Heading>
            <Panel.Body>{props.message}</Panel.Body>
        </Panel>
    )
};

export default MessagePanel;